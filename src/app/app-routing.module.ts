import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

export const routes: Routes = [
  {path: '', redirectTo: '/student-grading', pathMatch: 'full'},
  {
    path: 'student-grading',
    children: [
      {
        path: '',
        loadChildren: './student-grading/student-grading.module#StudentGradingModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
