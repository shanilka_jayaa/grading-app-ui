export class AnswerReviewModel {
  questionIndex: string;
  question: string;
  duration: string;
  studentAnswer: any[];
  correctAnswer: any[];
  grade: string;
}

