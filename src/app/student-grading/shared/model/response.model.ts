export interface ResponseModel {
  status: string;
  data: any;
}
