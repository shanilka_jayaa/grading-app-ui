export const enum QuestionGradeEnum {
  RIGHT = 'RIGHT',
  WRONG = 'WRONG',
  PARTIAL = 'PARTIAL',
  NOT_ANSWERED = 'NOT_ANSWERED'
}
