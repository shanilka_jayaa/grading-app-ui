import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SERVICE_URLS} from '../../../app.config';
import {Observable} from 'rxjs';
import {ResponseModel} from '../model/response.model';

@Injectable({
  providedIn: 'root'
})
export class AssignmentService {
  constructor(protected http: HttpClient) {
  }

  getAssignmentsByCourseId(courseId: string): Observable<any> {
    return this.http.get(SERVICE_URLS.GET_ALL_ASSIGNMENTS_BY_COURSE_ID + courseId);
  }
}
