import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SERVICE_URLS} from '../../../app.config';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {
  constructor(protected http: HttpClient) {
  }

  getQuestionsByAssignmentId(assignmentId: string): Observable<any> {
    return this.http.get(SERVICE_URLS.GET_ALL_QUESTIONS_BY_ASSIGNMENT + assignmentId);
  }
}
