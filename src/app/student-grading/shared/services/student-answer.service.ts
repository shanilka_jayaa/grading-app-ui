import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SERVICE_URLS} from '../../../app.config';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentAnswerService {
  constructor(protected http: HttpClient) {
  }

  getStudentAnswersByAssignmentIdAndUserId(assignmentId: string, userId: string): Observable<any> {
    return this.http.get(SERVICE_URLS.GET_STUDENT_ANSWER + assignmentId + '/user/' + userId);
  }
}
