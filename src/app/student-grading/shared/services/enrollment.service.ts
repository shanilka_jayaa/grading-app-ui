import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SERVICE_URLS} from '../../../app.config';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {ResponseModel} from '../model/response.model';

@Injectable({
  providedIn: 'root'
})
export class EnrollmentService {

  private studentCourseList = [];

  constructor(protected http: HttpClient) {
  }

  getEnrollmentByStudentId(studentId: string): Observable<any> {
    return this.http.get(SERVICE_URLS.GET_ALL_COURSES_BY_STUDENT_ID + studentId)
      .pipe(tap((res: ResponseModel) => this.setStudentCourseList(res.data)));
  }


  setStudentCourseList(value: any[]) {
    this.studentCourseList = value;
  }

  getSelectedCourseDetails(courseId: string) {
    const find = this.studentCourseList.find(value => value.courseId === courseId);
    return find;
  }
}
