import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SERVICE_URLS} from '../../../app.config';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GradeService {
  constructor(protected http: HttpClient) {
  }

  getAssignmentGradingByAssignmentIdAndUserId(assignmentId: string, userId: string): Observable<any> {
    return this.http.get(SERVICE_URLS.GET_ALL_GRADES_BY_ASSIGNMENT_AND_USER_ID + assignmentId + '/' + userId);
  }
}
