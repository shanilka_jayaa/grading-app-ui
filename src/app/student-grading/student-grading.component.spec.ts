import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentGradingComponent } from './student-grading.component';

describe('StudentGradingComponent', () => {
  let component: StudentGradingComponent;
  let fixture: ComponentFixture<StudentGradingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentGradingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentGradingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
