import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AssignmentService} from '../../shared/services/assignment.service';
import {ResponseModel} from '../../shared/model/response.model';
import {EnrollmentService} from '../../shared/services/enrollment.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {ViewGradesComponent} from '../view-grades/view-grades.component';
import {ReviewAnswerComponent} from '../review-answer/review-answer.component';

@Component({
  selector: 'app-view-assignments',
  templateUrl: './view-assignments.component.html',
  styleUrls: ['./view-assignments.component.css']
})
export class ViewAssignmentsComponent implements OnInit {
  courseId: string;
  userId: string;
  courseAssignmentsList = [];
  selectedCourse;

  // default properties
  bsModalRef: BsModalRef;

  constructor(private activatedRoute: ActivatedRoute, private assignmentService: AssignmentService,
              private router: Router, private enrollmentService: EnrollmentService,
              private modalService: BsModalService
  ) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (undefined !== params.course_id && undefined !== params.user_id) {
        this.courseId = params.course_id;
        this.userId = params.user_id;
        this.selectedCourse = this.enrollmentService.getSelectedCourseDetails(this.courseId);
        this.assignmentService.getAssignmentsByCourseId(this.courseId).subscribe((value: ResponseModel) => {
          this.courseAssignmentsList = value.data;
        }, error => {
          this.courseAssignmentsList = [];
          console.log(error);
        });
      } else {
        this.router.navigate(['']);
      }
    });
  }

  viewGradesOpenModal(assignment: any) {
    this.bsModalRef = this.modalService.show(ViewGradesComponent, {
      animated: true,
      backdrop: true,
      ignoreBackdropClick: true,
      initialState: {
        userId : this.userId,
        assignmentDetails: assignment
      }
    });
  }

  reviewAnswersOpenModal(assignment: any) {
    this.bsModalRef = this.modalService.show(ReviewAnswerComponent, {
      animated: true,
      backdrop: true,
      class: 'modal-lg',
      ignoreBackdropClick: true,
      initialState: {
        userId : this.userId,
        assignmentDetails: assignment
      }
    });
  }
}
