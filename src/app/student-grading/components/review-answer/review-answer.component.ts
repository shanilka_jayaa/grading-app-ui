import {Component, Input, OnInit} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {StudentAnswerService} from '../../shared/services/student-answer.service';
import {QuestionService} from '../../shared/services/question.service';
import {AnswerReviewModel} from '../../shared/model/answer-review.model';
import {ResponseModel} from '../../shared/model/response.model';
import {QuestionGradeEnum} from '../../shared/enums/question-grade.enum';

@Component({
  selector: 'app-review-answer',
  templateUrl: './review-answer.component.html',
  styleUrls: ['./review-answer.component.css']
})
export class ReviewAnswerComponent implements OnInit {
  @Input() assignmentDetails;
  @Input() userId;

  answerReviewList: AnswerReviewModel[] = [];


  constructor(public modalRef: BsModalRef, private studentAnswerService: StudentAnswerService,
              private questionService: QuestionService) {
  }

  ngOnInit() {
    this.processStudentQuestionSummery();
  }

  processStudentQuestionSummery() {
    this.questionService.getQuestionsByAssignmentId(this.assignmentDetails.assignmentId).subscribe((questions: ResponseModel) => {
      this.studentAnswerService.getStudentAnswersByAssignmentIdAndUserId(this.assignmentDetails.assignmentId, this.userId)
        .subscribe((studentAnswers: ResponseModel) => {

          const questionsList: any[] = questions.data;
          const studentAnswerList: any[] = studentAnswers.data;
          this.answerReviewList = [];
          questionsList.forEach(question => {
            const studentQuestionAnswer = studentAnswerList.find(value => value.questionId == question.index);
            const answerReviewModel = new AnswerReviewModel();
            answerReviewModel.questionIndex = question.index;
            answerReviewModel.question = question.description;
            answerReviewModel.duration = question.duration;
            const answers: any[] = question.answers;
            answerReviewModel.correctAnswer = answers.filter(value => value.isCorrectAnswer);
            answerReviewModel.studentAnswer = [];
            studentQuestionAnswer.answerIndexes.forEach(answerIndex => {
              answerReviewModel.studentAnswer.push(answers.find(value => value.answerIndex == answerIndex));
            });
            if (answerReviewModel.studentAnswer.length === 1) {
              answerReviewModel.grade = answerReviewModel.studentAnswer[0].isCorrectAnswer ?
                (answerReviewModel.correctAnswer.length === answerReviewModel.studentAnswer.length ?
                  QuestionGradeEnum.RIGHT : QuestionGradeEnum.PARTIAL)
                : QuestionGradeEnum.WRONG;
            } else if (answerReviewModel.studentAnswer.length > 1) {
              answerReviewModel.grade = QuestionGradeEnum.WRONG;
              answerReviewModel.studentAnswer.forEach(value => {
                if (value.isCorrectAnswer) {
                  answerReviewModel.grade = QuestionGradeEnum.RIGHT;
                } else {
                  answerReviewModel.grade = (answerReviewModel.grade === QuestionGradeEnum.RIGHT ?
                    QuestionGradeEnum.PARTIAL : QuestionGradeEnum.WRONG);
                }
              });
            } else {
              answerReviewModel.grade = QuestionGradeEnum.NOT_ANSWERED;
            }
            this.answerReviewList.push(answerReviewModel);
          });
        }, error => {
          console.log(error);
          this.answerReviewList = [];
        });
    }, error => {
      console.log(error);
      this.answerReviewList = [];
    });
  }

}
