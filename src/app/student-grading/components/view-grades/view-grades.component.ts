import {Component, Input, OnInit} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {GradeService} from '../../shared/services/grade.service';
import {ResponseModel} from '../../shared/model/response.model';

@Component({
  selector: 'app-view-grades',
  templateUrl: './view-grades.component.html',
  styleUrls: ['./view-grades.component.css']
})
export class ViewGradesComponent implements OnInit {

  @Input() assignmentDetails;
  @Input() userId;

  gradingList = [];

  gradeSummery = {
    rightAnswers: 0,
    wrongAnswers: 0,
    partialAnswers: 0
  };

  constructor(public modalRef: BsModalRef, private gradeService: GradeService) {
  }

  ngOnInit() {
    this.gradeService.getAssignmentGradingByAssignmentIdAndUserId(this.assignmentDetails.assignmentId, this.userId)
      .subscribe((value: ResponseModel) => {
        this.gradingList = value.data;
        value.data.forEach(grade => {
          if (grade.grade === 'RIGHT') {
            this.gradeSummery.rightAnswers += 1;
          } else if (grade.grade === 'PARTIAL') {
            this.gradeSummery.partialAnswers += 1;
          } else {
            this.gradeSummery.wrongAnswers += 1;
          }
        });
      }, error => {
        console.log(error);
        this.gradingList = [];
      });
  }

}
