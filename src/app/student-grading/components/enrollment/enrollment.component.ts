import {Component, OnInit} from '@angular/core';
import {EnrollmentService} from '../../shared/services/enrollment.service';
import {ResponseModel} from '../../shared/model/response.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-enrollment',
  templateUrl: './enrollment.component.html',
  styleUrls: ['./enrollment.component.css']
})
export class EnrollmentComponent implements OnInit {
  studentEnrollments = [];
  userId = '5f672be5e911e357c697ee72';


  constructor(private enrollmentService: EnrollmentService, private router: Router) {
  }

  ngOnInit() {
    this.enrollmentService.getEnrollmentByStudentId(this.userId).subscribe((value: ResponseModel) => {
      this.studentEnrollments = value.data;
    });
  }

  getDateString(date: string) {
    return date.substring(0, 10);
  }

  goToViewAssignments(courseId) {
    this.router.navigate(['student-grading/view-assignment/' + courseId + '/' + this.userId]);
  }

}
