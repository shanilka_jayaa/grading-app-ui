import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StudentGradingComponent} from './student-grading.component';
import {EnrollmentComponent} from './components/enrollment/enrollment.component';
import {ViewAssignmentsComponent} from './components/view-assignments/view-assignments.component';
import {ViewGradesComponent} from './components/view-grades/view-grades.component';
import {ReviewAnswerComponent} from './components/review-answer/review-answer.component';
import {HttpClientModule} from '@angular/common/http';
import {StudentGradingRoutingModule} from './student-grading-routing.module';
import {ModalModule} from 'ngx-bootstrap';


@NgModule({
  declarations: [
    StudentGradingComponent,
    EnrollmentComponent,
    ViewAssignmentsComponent,
    ViewGradesComponent,
    ReviewAnswerComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    StudentGradingRoutingModule,
    ModalModule.forRoot(),
  ],
  entryComponents: [
    ViewGradesComponent,
    ReviewAnswerComponent
  ]
})
export class StudentGradingModule {
}
