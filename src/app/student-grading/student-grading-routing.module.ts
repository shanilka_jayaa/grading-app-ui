import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {EnrollmentComponent} from './components/enrollment/enrollment.component';
import {ViewAssignmentsComponent} from './components/view-assignments/view-assignments.component';

const routes: Routes = [
  {
    path: '',
    component: EnrollmentComponent
  },
  {
    path: 'view-assignment/:course_id/:user_id',
    component: ViewAssignmentsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentGradingRoutingModule {

}
